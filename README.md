# NodeJsScan analyzer

GitLab Analyzer for NodeJS projects.
It's based on [the rules](https://github.com/ajinabraham/NodeJsScan/blob/master/core/rules.xml)
and signatures of [NodeJsScan](https://github.com/ajinabraham/NodeJsScan).
It only processes `.js` files even though [NodeJsScan](https://github.com/ajinabraham/NodeJsScan) can handle other file types.
Also, this analyzer uses the [Closure Compiler](https://developers.google.com/closure/compiler/)
to remove comments that could otherwise match the signatures of NodeJsScan's rules.

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

## Updating the underlying Scanner

The NodeJs-Scan analyzer does not directly rely on the NodeJs-Scan scanner but only uses its rules DB.

To update NodeJs-Scan rules, please follow these steps:
- update `NODEJS_SCAN_VERSION` in the `Dockerfile`
- check the content of https://raw.githubusercontent.com/ajinabraham/NodeJsScan/v${NODEJS_SCAN_VERSION}/core/rules.xml (replace `NODEJS_SCAN_VERSION`)
- if new rules are present, add them to the `rulesIdentifiers` map in `rules.go` to provide a corresponding Identifier.
- if an existing rules has been renamed, keep the old one and add the new one with the same ID. This will ensure backward compatibility with previous reports.
- build and test. Execution will fail if a rule doesn't have a matching Identifier.


## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see the [LICENSE](LICENSE) file.
