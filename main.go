package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2/plugin"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "NodeJsScan analyzer for GitLab SAST"
	app.Author = "GitLab"

	cfg := command.Config{Match: plugin.Match}
	app.Commands = []cli.Command{
		Run(),
		command.Search(cfg),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
