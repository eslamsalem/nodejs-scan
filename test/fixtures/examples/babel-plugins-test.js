// Add XSS vulnerability
var main = express();

if (!module.parent) main.use(logger('dev'));

main.get('/:sub', function(req, res){
  res.send('requested ' + req.params.sub);
});



// @babel/plugin-proposal-optional-chaining
const obj = {
  foo: {
    bar: {
      baz: 42,
    },
  },
};
const baz = obj?.foo?.bar?.baz; // 42
const safe = obj?.qux?.baz; // undefined

// @babel/plugin-proposal-export-default-from
export v from 'mod';

// @babel/plugin-proposal-function-bind
const box = {
  weight: 2,
  getWeight() { return this.weight; },
};
const { getWeight } = box;
console.log(box.getWeight()); // prints '2'
const bigBox = { weight: 10 };
console.log(bigBox::getWeight()); // prints '10'

// @babel/plugin-proposal-logical-assignment-operators
a ||= b;
obj.a.b ||= c;

// @babel/plugin-proposal-nullish-coalescing-operator
var foo = object.foo ?? "default";

// @babel/plugin-proposal-do-expressions
let a = do {
  if(x > 10) {
    'big';
  } else {
    'small';
  }
};

// @babel/plugin-proposal-decorators
@annotation
class MyClass { }

function annotation(target) {
   target.annotated = true;
}

// @babel/plugin-proposal-function-sent
function* generator() {
    console.log("Sent", function.sent);
    console.log("Yield", yield);
}

const iterator = generator();
iterator.next(1); // Logs "Sent 1"
iterator.next(2); // Logs "Yield 2"

// @babel/plugin-proposal-export-namespace-from
export * as ns from 'mod';

// @babel/plugin-proposal-numeric-separator
let budget = 1_000_000_000_000;
console.log(budget === 10 ** 12); // true

// @babel/plugin-proposal-throw-expressions
function test(param = throw new Error('required!')) {
  const test = param === true || throw new Error('Falsy!');
}

// @babel/plugin-proposal-class-properties
class Bork {
    static a = 'foo';
    static b;

    x = 'bar';
    y;
}
